package com.example.dto;

import com.example.entities.Post;
import com.example.util.StringUtil;
import com.example.view.PostView;
import org.dozer.CustomConverter;

/**
 * Created by sonat on 13.08.2016.
 */
public class PostContentToBase64Converter implements CustomConverter {

    public Object convert(Object destination, Object source,  Class destClass, Class sourceClass) {
//        if (source == null) {
//            return null;
//        }
//        if (!String.class.isAssignableFrom(destClass)) {
//            return null;
//        }
        PostView dest = null;
        try {

            if (destination == null) {
                dest = new PostView();
            } else {
                dest = (PostView) destination;
            }

            if (((Post) source).getDocument() == null) {
                dest.setDocument(null);
                return dest;
            }
            if (((Post) source).getDocument().getContent() == null) {
                dest.setDocument(null);
                return dest;
            }

            byte[] content = ((Post) source).getDocument().getContent();
            String result = StringUtil.getBase64(content);

            dest.setDocument(result);


        } catch (Exception e) {
            e.printStackTrace();

        }
        return dest;
    }
}
