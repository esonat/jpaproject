package com.example.dto;

import com.example.entities.Document;
import com.example.util.StringUtil;
import org.dozer.DozerConverter;

/**
 * Created by sonat on 13.08.2016.
 */
public class DocumentToStringConverter
        extends DozerConverter<Document,String> {

    public DocumentToStringConverter() {
        super(Document.class, String.class);
    }

    public String convertTo(Document source, String destination){
        String base64=null;
        try {
            byte[] content = source.getContent();
            base64 = StringUtil.getBase64(content);
        }catch (Exception e){
            e.printStackTrace();
        }
        return base64;
    }

    public Document convertFrom(String source, Document destination) {
        Document document=new Document();
        return document;
    }

}
