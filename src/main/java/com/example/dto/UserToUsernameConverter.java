package com.example.dto;

import com.example.entities.User;
import com.example.entities.UserLoginInfo;
import org.dozer.DozerConverter;


public class UserToUsernameConverter extends DozerConverter<User,String> {

    public UserToUsernameConverter() {
        super(User.class, String.class);
    }

    public String convertTo(User user, String s) {
        String username=null;
        try{
            username=user.getUserLoginInfo().getUsername();

        }catch (Exception e){
            e.printStackTrace();
        }
        return username;
    }

    public User convertFrom(String s, User user) {
        user=new User();
        try{
            UserLoginInfo userLoginInfo=new UserLoginInfo();
            userLoginInfo.setUsername(s);

            user.setUserLoginInfo(userLoginInfo);

        }catch (Exception e){
            e.printStackTrace();
        }
        return user;
    }
}
