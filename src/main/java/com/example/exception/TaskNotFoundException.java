package com.example.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TaskNotFoundException extends RuntimeException {
    Logger logger= LoggerFactory.getLogger(ClientNotFoundException.class);

    public TaskNotFoundException(String message) {
        super(message);
        logger.warn("Task not found!");
    }

    public TaskNotFoundException(Throwable cause) {
        super(cause);
    }

    public TaskNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
