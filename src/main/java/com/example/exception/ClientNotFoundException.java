package com.example.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by sonat on 14.08.2016.
 */
public class ClientNotFoundException extends RuntimeException {
    Logger logger= LoggerFactory.getLogger(ClientNotFoundException.class);

    public ClientNotFoundException(String message) {
        super(message);
        logger.warn("Client not found!");
    }

    public ClientNotFoundException(Throwable cause) {
        super(cause);
    }

    public ClientNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
