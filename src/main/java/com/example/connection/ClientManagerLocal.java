package com.example.connection;

/**
 * Created by sonat on 14.08.2016.
 */
public interface ClientManagerLocal {
    void addClient(String clientId, String port, String username, String role);

    void removeClient(String clientId);
}
