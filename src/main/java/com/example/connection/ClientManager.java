package com.example.connection;

import com.example.entities.*;
import com.example.exception.ClientNotFoundException;
import com.example.exception.TaskNotFoundException;
import com.example.exception.UserNotFoundException;
import com.example.service.BlogService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


@ApplicationScoped
public class ClientManager{
    private static Logger logger = LoggerFactory.getLogger(ClientManager.class);
    private static int CLIENT_ROLE_COUNT=3;
    private static int DISCONNECT_TIME=14;

    private List<Client> connectedClients;

    @EJB
    BlogService blogService;

    @PostConstruct
    public void init(){
        connectedClients=new ArrayList<Client>();
    }


    public void addClient(String clientId, String port, String username, String role) throws UserNotFoundException{

        try {
            if(clientExists(clientId)) {
                Client updated=getClientById(clientId);

                updated.setLatestMessage(new Date());
                connectedClients.set(getClientIndex(clientId),updated);
                blogService.updateClient(updated);
                logger.info("CLIENT UPDATED: LATEST MESSAGE:"+new Date());

                return;
            }

            Client client = new Client();
            client.setId(clientId);
            client.setClientRole(role);
            client.setPort(Integer.parseInt(port));
            client.setStatus(true);
            client.setLatestActivity(new Date());
            client.setLatestMessage(new Date());

            User user = blogService.getUserByUsername(username);

            connectedClients.add(client);
            blogService.addClient(client, user);

        }catch (Exception e){
            logger.warn("ADDCLIENT: "+e.getMessage()+e.getCause());
        }
    }
    public Client getClientById(String clientId){
        for (int index = 0; index < connectedClients.size(); index++) {
            Client connectedClient = connectedClients.get(index);

            if (connectedClient.getId().equals(clientId)) return connectedClient;
        }

        return null;
    }

    public int getClientIndex(String clientId){
        for (int index = 0; index < connectedClients.size(); index++) {
            Client connectedClient = connectedClients.get(index);
            if (connectedClient.getId().equals(clientId)) return index;
        }
        return -1;
    }

    public boolean clientExists(String clientId){
        for (int index = 0; index < connectedClients.size(); index++) {
            Client connectedClient = connectedClients.get(index);
            if (connectedClient.getId().equals(clientId)) return true;
        }
        return false;
    }

    public boolean isClientListEmpty(){
        return (connectedClients.size()==0);
    }

    public void removeClients(List<String> list) {
        try {
            List<Integer> toRemove=new ArrayList<Integer>();

            for(int index=0;index<list.size();index++){
                String clientId=list.get(index);

                blogService.removeClient(clientId);
                toRemove.add(getClientIndex(clientId));
            }

            connectedClients.removeAll(toRemove);


            logger.info("CLIENTS REMOVED");

        }catch (Exception e){
            logger.warn(e.getMessage()+e.getCause()+"Remove Client");
        }
    }

    public void updateClient(String clientId){
        Client client=getClientById(clientId);

        if(client==null){
            logger.info("CLIENT WITH ID:"+clientId+" NOT FOUND!");
            return;
        }

        try {
            client.setLatestActivity(new Date());
            blogService.updateClient(client);

        }catch (ClientNotFoundException e){
            logger.warn("Update Client:"+e.getMessage());
            return;
        }
    }

    public void updateTask(String type) throws TaskNotFoundException,Exception{
        Task task = blogService.getTaskByType(type);
        task.setLatestActivity(new Date());
        blogService.updateTask(task);
    }

    public void removeDisconnectedClients() {
        try {
            if(blogService.getTaskByType(TaskType.TASK_REMOVEDISCONNECTED.toString())==null) {

                Task item = new Task();
                item.setType(TaskType.TASK_REMOVEDISCONNECTED.toString());
                item.setLatestActivity(null);
                item.setStartTime(new Date());

                Task task = blogService.addTask(item);
            }
        }
        catch(Exception e){
             logger.warn("REMOVE DISCONNECTED CLIENTS:" + e.getCause() + e.getMessage());
        }

            Runnable runnable = new Runnable() {
                public void run() {

                    try {
                        if (connectedClients!=null && connectedClients.size()>0) {
                            logger.info("CLIENT ITEM:"+connectedClients.get(0).getId());

                            Date now = new  Date();

                            List<Integer> toRemove=new ArrayList<Integer>();

                            for (int index=0;index<connectedClients.size();index++) {
                                Client connectedClient=connectedClients.get(index);

                                long difference = (now.getTime() - connectedClient.getLatestMessage().getTime()) / 1000;
                                if (difference > DISCONNECT_TIME) toRemove.add(index);
                            }


                            connectedClients.removeAll(toRemove);
                           // blogService.removeClient();
                            updateTask(TaskType.TASK_REMOVEDISCONNECTED.toString());

//
//                                if(toRemove.size()!=0) {
//                                    removeClients(toRemove);
//                                    updateTask(TaskType.TASK_REMOVEDISCONNECTED.toString());
//                                }

                            toRemove.clear();
                            }
                    }catch (Exception e) {
                        logger.warn("REMOVE DISCONNECTED CLIENTS:" + e.getCause() + e.getMessage());
                    }
                }
            };

                ScheduledExecutorService service =Executors.newSingleThreadScheduledExecutor();
                service.scheduleAtFixedRate(runnable,0,15,TimeUnit.SECONDS);
    }


    public void assignClientRole(){

       try {
           if(blogService.getTaskByType(TaskType.TASK_ASSIGNROLE.toString())==null) {

               Task item = new Task();
               item.setType(TaskType.TASK_ASSIGNROLE.toString());
               item.setStartTime(new Date());
               item.setLatestActivity(null);

               Task task = blogService.addTask(item);
           }
       }catch (Exception e){
           logger.warn("ASSIGNCLIENTROLE: "+e.getCause()+e.getMessage());
       }
            Runnable runnable = new Runnable() {
                public void run() {
                  if(connectedClients!=null && connectedClients.size()>0) {

                        Random random = new Random();

                        int r_Id = random.nextInt(connectedClients.size());
                        int r_Role = random.nextInt(CLIENT_ROLE_COUNT);

                        if(connectedClients.size()==1) r_Id=0;

                        try {
                            Client client = connectedClients.get(r_Id);
                            String role = ClientRole.values()[r_Role].toString();
                            client.setClientRole(role);

                            updateClient(client.getId());
                            updateTask(TaskType.TASK_ASSIGNROLE.toString());

                        }catch (Exception e) {
                            logger.warn(e.getCause() + e.getMessage());

                        }
                    }
                }
            };

            ScheduledExecutorService service =Executors.newSingleThreadScheduledExecutor();
            service.scheduleAtFixedRate(runnable, 0, 10, TimeUnit.SECONDS);
        }
}
