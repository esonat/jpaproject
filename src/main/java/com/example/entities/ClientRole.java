package com.example.entities;

/**
 * Created by sonat on 14.08.2016.
 */
public enum ClientRole {
    POST,
    COMMENT,
    CATEGORY
};
