package com.example.entities;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by sonat on 08.08.2016.
 */
@Entity
public class Phone implements Serializable {
    private static final long serialVersionUID = -5558920121569963193L;
    @Id
    String num;

    String type;

    @ManyToOne
    @JoinColumn(name="USER_ID")
    private User user;

    public String getNum() {
        return num;
    }
    public void setNum(String num) {
        this.num = num;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String toString() {
        return "Phone num: " + num + " type: " + type;
    }
}
