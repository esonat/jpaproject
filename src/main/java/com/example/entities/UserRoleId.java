package com.example.entities;

import java.io.Serializable;

/**
 * Created by sonat on 10.08.2016.
 */
public class UserRoleId implements Serializable {
    private int user;
    private int role;

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public int getUser() {
        return user;
    }

    public void setUser(int user) {
        this.user = user;
    }
}
