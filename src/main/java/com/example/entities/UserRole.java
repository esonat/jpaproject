package com.example.entities;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by sonat on 09.08.2016.
 */

@Entity
@Table(name="USER_ROLES")
@IdClass(UserRoleId.class)
public class UserRole implements Serializable {
    private static final long serialVersionUID = -8846960848206963247L;
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private int id;
//
    @Id
    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name="USER_ID")
    private User user;

    @Id
    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name="ROLE_ID")
    private Role role;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
