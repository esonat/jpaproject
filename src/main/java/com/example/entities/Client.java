package com.example.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name="CLIENT")
public class Client implements Serializable{
    @Id
    private String id;

    @Column(name="PORT")
    private int port;

    @Column(name="STATUS")
    private boolean status;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="USER_ID")
    private User user;

    @Column(name="ROLE")
    private String clientRole;

    @Temporal(TemporalType.TIME)
    @Column(name="LAST_ACTIVITY_TIME")
    private Date latestActivity;

    @Temporal(TemporalType.TIME)
    @Column(name="LATEST_MESSAGE")
    private Date latestMessage;

    public Client() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getLatestActivity() {
        return latestActivity;
    }

    public void setLatestActivity(Date latestActivity) {
        this.latestActivity = latestActivity;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getClientRole() {
        return clientRole;
    }

    public void setClientRole(String clientRole) {
        this.clientRole = clientRole;
    }

    public Date getLatestMessage() {
        return latestMessage;
    }

    public void setLatestMessage(Date latestMessage) {
        this.latestMessage = latestMessage;
    }
}
