package com.example.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sonat on 08.08.2016.
 */

@Entity
@NamedQueries({
        @NamedQuery(name="Country.getByName",
        query = "SELECT c FROM Country c WHERE c.name= :name")
})
public class Country implements Serializable{
    private static final long serialVersionUID = -7309419869406902634L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name="COUNTRY_NAME")
    private String name;

    @OneToMany(mappedBy = "country",fetch = FetchType.EAGER,cascade = CascadeType.REMOVE)
    @OrderBy("name ASC")
    private List<City> cities;

    public Country(){
        cities=new ArrayList<City>();
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<City> getCities() {
        return cities;
    }

    public void setCities(List<City> cities) {
        this.cities = cities;
    }
}
