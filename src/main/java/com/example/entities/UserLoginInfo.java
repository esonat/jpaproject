package com.example.entities;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by sonat on 08.08.2016.
 */
@Embeddable
public class UserLoginInfo implements Serializable {

    private static final long serialVersionUID = 1786448500113798810L;

    @Column(name="USERNAME",unique = true)
    private String username;

    @Column(name="PASSWORD")
    @Size(min=3,max=5,message = "Password must be between 3 and 5 characters")
    private String password;

    @Temporal(TemporalType.TIME)
    private Date lastLoginTime;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }
}
