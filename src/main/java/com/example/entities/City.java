package com.example.entities;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by sonat on 08.08.2016.
 */

@Entity
@NamedQueries({
        @NamedQuery(name="City.getByName",
                query = "SELECT c FROM City c WHERE c.name= :name")
})
public class City implements Serializable {
    private static final long serialVersionUID = 4610495354721833594L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name="CITY_NAME")
    private String name;

    @ManyToOne
    @JoinColumn(name="COUNTRY_ID")
    private Country country;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
