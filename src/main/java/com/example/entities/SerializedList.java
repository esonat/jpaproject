package com.example.entities;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by sonat on 09.08.2016.
 */
public class SerializedList extends ArrayList<Role> implements Serializable{
    private static final long serialVersionUID = 4334650710527995282L;
}
