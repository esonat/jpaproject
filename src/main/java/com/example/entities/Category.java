package com.example.entities;

import javax.inject.Named;
import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


@Entity
@NamedQueries({
        @NamedQuery(name="Category.findByName",
        query = "SELECT c FROM Category c WHERE c.name = :name")
})
public class Category implements Serializable{
    private static final long serialVersionUID = 118993491381181460L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Version
    private int version;

    @Column(name="CATEGORY_NAME")
    private String name;

    @OneToMany(mappedBy = "category",fetch = FetchType.EAGER,cascade = CascadeType.REMOVE)
    private List<Post> posts;

    public Category(){
        posts=new ArrayList<Post>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Post> getPosts() {
        return posts;
    }

    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }
}
