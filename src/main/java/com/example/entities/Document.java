package com.example.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by sonat on 12.08.2016.
 */
@Entity
@Table(name="DOCUMENT")
public class Document implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Version
    private int version;

    @Column(name="TYPE")
    private DocumentType type;

    @Column(name="STATUS")
    private DocumentStatus status;

    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Column(name="CONTENT")
    private byte[] content;

    @OneToMany(mappedBy = "document",fetch = FetchType.LAZY)
    private List<Post> posts;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public DocumentType getType() {
        return type;
    }

    public void setType(DocumentType type) {
        this.type = type;
    }

    public DocumentStatus getStatus() {
        return status;
    }

    public void setStatus(DocumentStatus status) {
        this.status = status;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public List<Post> getPosts() {
        return posts;
    }

    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }
}
