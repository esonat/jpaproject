package com.example.entities;

/**
 * Created by sonat on 12.08.2016.
 */
public enum DocumentStatus {
    PENDING,
    APPROVED,
    REJECTED
};
