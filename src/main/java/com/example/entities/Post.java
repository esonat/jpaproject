package com.example.entities;

import com.example.validation.PostUsername;
import org.eclipse.persistence.annotations.UnionPartitioning;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@NamedQueries({
        @NamedQuery(name="Post.findAll",
            query="SELECT p FROM Post p"),
        @NamedQuery(name="Post.findComments",
            query="select p.comments from Post p"),
        @NamedQuery(name="Post.findByUserName",
            query="SELECT p FROM Post p WHERE p.user.userinfo.name = :name")
})
public class Post implements Serializable {
    private static final long serialVersionUID = 4535704024413259871L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Version
    private int version;

    @NotNull
    @Column(name="TEXT",length = 255,unique = true)
    private String text;

    @Column(name="DATETIME")
    @Temporal(TemporalType.TIME)
    private Date datetime;


    @ManyToOne(fetch = FetchType.EAGER,cascade = CascadeType.PERSIST)
    @JoinColumn(name="DOCUMENT_ID")
    private Document document;


    @Size(max=2,message = "Post can not have more than 2 comments")
    @OneToMany(mappedBy = "post",cascade = CascadeType.REMOVE,fetch = FetchType.EAGER)
    @OrderBy("depth ASC")
    private List<Comment> comments;

    @PostUsername
    @ManyToOne
    @JoinColumn(name="USER_ID")
    private User user;

    @ManyToOne(fetch = FetchType.EAGER,cascade = CascadeType.MERGE)
    @JoinColumn(name="CATEGORY_ID")
    private Category category;


    public Post(){
        comments=new ArrayList<Comment>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getDatetime() {
        return datetime;
    }

    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Document getDocument() {
        return document;
    }

    public void setDocument(Document document) {
        this.document = document;
    }
}
