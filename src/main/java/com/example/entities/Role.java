package com.example.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sonat on 09.08.2016.
 */
@Entity
public class Role implements Serializable{
    private static final long serialVersionUID = 6092198373388527556L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Version
    private int version;

    @Column(name="ROLE_NAME")
    private String name;

//    @ManyToMany
//    @JoinTable(name="USER_ROLES",
//        joinColumns =
//            @JoinColumn(name="ROLE_ID",referencedColumnName = "ID"),
//        inverseJoinColumns =
//            @JoinColumn(name="USER_ID",referencedColumnName = "ID"))

    @OneToMany(fetch = FetchType.EAGER,mappedBy = "role",cascade = CascadeType.ALL)
    private List<UserRole> userRoles=new ArrayList<UserRole>();

    public Role(){
        userRoles=new ArrayList<UserRole>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<UserRole> getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(List<UserRole> users) {
        this.userRoles= users;
    }
}
