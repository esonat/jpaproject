package com.example.entities;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by sonat on 15.08.2016.
 */
@Entity
@Table(name="TASK")
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name="TYPE")
    private String type;

    @Temporal(TemporalType.TIME)
    private Date startTime;

    @Temporal(TemporalType.TIME)
    @Column(name="LAST_ACTIVITY_TIME")
    private Date latestActivity;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getLatestActivity() {
        return latestActivity;
    }

    public void setLatestActivity(Date latestActivity) {
        this.latestActivity = latestActivity;
    }
}

