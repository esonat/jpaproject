package com.example.entities;

/**
 * Created by sonat on 15.08.2016.
 */
public enum TaskType {
    TASK_ASSIGNROLE,        /*Assign client roles*/
    TASK_REMOVEDISCONNECTED /*Remove disconnected clients*/
}
