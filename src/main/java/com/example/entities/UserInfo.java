package com.example.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sonat on 08.08.2016.
 */

@Embeddable
@Access(AccessType.FIELD)
public class UserInfo implements Serializable {

    private static final long serialVersionUID = -625550206423185398L;
    @Column(name="NAME")
    private String name;

    @Embedded
    private Address address;

    @OneToMany(mappedBy = "user",fetch = FetchType.EAGER,cascade = CascadeType.REMOVE)
    private List<Phone> phones;

    public UserInfo(){
        phones=new ArrayList<Phone>();
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public List<Phone> getPhones() {
        return phones;
    }

    public void setPhones(List<Phone> phones) {
        this.phones = phones;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
