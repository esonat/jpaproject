package com.example.entities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by sonat on 08.08.2016.
 */
@Entity
public class Comment implements Serializable{
    private static final long serialVersionUID = -7028777680195835731L;
    public static Logger logger= LoggerFactory.getLogger(Comment.class);

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Version
    private int version;

    @Column(name="TEXT",length = 200)
    private String text;

    @Column(name="DATETIME")
    @Temporal(TemporalType.TIME)
    private Date datetime;

    @ManyToOne
    @JoinColumn(name = "USER_ID")
    private User user;

    @ManyToOne
    @JoinColumn(name="POST_ID")
    private Post post;

    @ManyToOne
    @JoinColumn(name="PARENT",nullable = true)
    private Comment parent;

    @OneToMany(mappedBy = "parent",cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    private List<Comment> children;

    @Column(name="DEPTH",columnDefinition="int(11) default 0")
    private int depth;


    @PrePersist
    public void prePersist(){
        setText("PRE PERSIST");
       logger.info("Comment "+getText());
    }
    @PreRemove
    public void preRemove(){
        setText("PRE REMOVE");
        logger.info("Comment "+getText());
    }

    public Comment(){
        children=new ArrayList<Comment>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getDatetime() {
        return datetime;
    }

    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public Comment getParent() {
        return parent;
    }

    public void setParent(Comment parent) {
        this.parent = parent;
    }

    public int getDepth() {
        return depth;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }

    public List<Comment> getChildren() {
        return children;
    }

    public void setChildren(List<Comment> children) {
        this.children = children;
    }
}
