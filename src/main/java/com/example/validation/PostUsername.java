package com.example.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by sonat on 10.08.2016.
 */

@Constraint(validatedBy = {PostUsernameValidator.class})
@Target({ElementType.METHOD,ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface PostUsername {
    String message() default "User with username deneme can not post";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
