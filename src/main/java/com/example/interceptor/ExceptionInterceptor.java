package com.example.interceptor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

/**
 * Created by sonat on 12.08.2016.
 */

public class ExceptionInterceptor {
    Logger logger= LoggerFactory.getLogger(ExceptionInterceptor.class);

    @AroundInvoke
    public Object methodInterceptor(InvocationContext ctx) throws Exception {
        logger.info("EXCEPTION INTERCEPTOR CALLED");

        return ctx.proceed();
    }
}
