package com.example.service;

import com.example.entities.*;
import com.example.exception.TaskNotFoundException;
import com.example.exception.UserNotFoundException;

import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.Date;
import java.util.List;

/**
 * Created by sonat on 08.08.2016.
 */

//@EJB(beanName = "BlogService", name = "BlogService")


//EJB(beanInterface = BlogServiceLocal.class,name="BlogService",beanName = "BlogService")
@Stateless
public class BlogService{
    public static final long REFRESH_THRESHOLD = 300000;


    @PersistenceContext(unitName = "BlogPersistence")
    private EntityManager em;


    public Task addTask(Task task) throws Exception{
        if(getTaskByType(task.getType())!=null)
            throw new Exception("TASK WITH TYPE "+task.getType()+"EXISTS");

        em.persist(task);
        return task;
    }

    public void removeTask(Task task) throws TaskNotFoundException,Exception{
        task=em.find(Task.class,task.getId());
        em.remove(task);
    }

    public Task updateTask(Task task) throws TaskNotFoundException,Exception{
    //    Task updated=em.find(Task.class,task.getId());
        em.merge(task);

        return task;
    }

    public Task getTaskByType(String type){
        List<Task> list=em.createQuery("SELECT t FROM Task t WHERE t.type= :type")
                .setParameter("type",type)
                .getResultList();

        if(list==null || list.size()==0) return null;

        return list.get(0);
    }

    public void addClient(Client client, User user){
        client.setUser(user);

        em.persist(client);
    }

    public void removeClient(String clientId) throws Exception{
        Client client=em.find(Client.class,clientId);
        em.remove(client);
    }

    public void updateClient(Client client){
        em.merge(client);
    }

    public List<Category> getAllCategories(){
        return (List<Category>)em.createQuery("SELECT c FROM Category c")
                .getResultList();
    }
    public void  addCategory(String name){
        Category category=new Category();
        category.setName(name);

        em.persist(category);
    }

    public Document addDocument(Document document) {
        List<Document> list = (List<Document>) em.createQuery("SELECT d FROM Document d WHERE d.content = :contentString", Document.class)
                .setParameter("contentString", document.getContent())
                .getResultList();


        if (list != null && list.size() > 0) return document;

        document.setStatus(DocumentStatus.APPROVED);
        em.persist(document);

        return document;
    }

    public List<Document> getAllDocuments() {
        return (List<Document>) em.createQuery("SELECT d FROM Document d").getResultList();
    }

    public void addCategory(Category category) {
        em.persist(category);
    }

    public Category findCategoryByName(String name) {

        TypedQuery<Category> query = em.createQuery(
                "SELECT c FROM Category c WHERE c.name = ?1",
                Category.class);
        query.setParameter(1, name);
        query.setMaxResults(1);

        List<Category> result = query.getResultList();

        return result.get(0);
    }

    public void addRole(Role role) {
        em.persist(role);
    }

    public Role findRoleByName(String name) {
        List<Role> list = (List<Role>) em.createQuery("SELECT r FROM Role r WHERE r.name = :name")
                .setParameter("name", name)
                .getResultList();

        if (list == null ||
                list.size() == 0) return null;

        return list.get(0);
    }

    public void addUser(User user, List<Role> roleList, List<Phone> phones) {

        try {
            user.getUserinfo().getPhones().addAll(phones);

            for (Role role : roleList) {
                boolean exists = (findRoleByName(role.getName()) != null);

                if (exists)
                    role = findRoleByName(role.getName());

                UserRole userRole = new UserRole();
                userRole.setUser(user);
                userRole.setRole(role);
                //user.getUserRoles().add(userRole);
                role.getUserRoles().add(userRole);

                if (!exists) {
                    em.persist(role);
                }
            }

            for (Phone phone : phones) {
                phone.setUser(user);
                em.persist(phone);
            }
            em.persist(user);

        } catch (EJBException e) {
            e.printStackTrace();
        }
    }

    public void addPostComment(int postId, Comment comment) {
        Post post = em.find(Post.class, postId);
        post.getComments().add(comment);
        comment.setPost(post);
        em.persist(post);
    }

    public void addChildComment(int parentId, Comment comment) {
        Comment parent = em.find(Comment.class, parentId);
        parent.getChildren().add(comment);
        comment.setParent(parent);
        em.persist(parent);
    }

    public void addCountry(Country country) {
        em.persist(country);
        //return country.getId();
    }

    public Country getCountryByName(String name) {
        TypedQuery<Country> query =
                em.createNamedQuery("Country.getByName", Country.class)
                        .setParameter("name", name);

        List<Country> results = query.getResultList();

        return results.get(0);
    }

    public void addCity(int countryId, City city) {
        Country country = em.find(Country.class, countryId);
        country.getCities().add(city);
        city.setCountry(country);

        em.persist(city);
    }

    public City getCityByName(String name) {
        return (City) em.createQuery("SELECT c FROM City c WHERE c.name = :name")
                .setParameter("name", name)
                .getResultList().get(0);

    }

    public User getUserByName(String name) {
        return (User) em.createQuery("SELECT u FROM User u WHERE u.userinfo.name = :name")
                .setParameter("name", name)
                .getResultList().get(0);
    }

    public User getUserByUsername(String username){
            List<User> users=(List<User>)em.createQuery("SELECT u FROM User u WHERE u.userLoginInfo.username = :username")
                    .setParameter("username", username)
                    .getResultList();

        if(users==null ||
                users.size()==0) throw new UserNotFoundException("User with "+username+ "not found!");

            return users.get(0);
    }

    public void addPost(Post post, String categoryName, String username, Document document) {
        List<Category> list = (List<Category>)
                em.createQuery("SELECT c FROM Category c WHERE c.name =  :name")
                        .setParameter("name", categoryName)
                        .getResultList();
        Category category = null;
        ;

        if (list == null ||
                list.size() == 0) {
            category = new Category();
            category.setName(categoryName);

        } else if (list != null && list.size() > 0) {
            category = list.get(0);
        }

//        List<User> users = (List<User>) em.createQuery("SELECT u FROM User u WHERE u.userLoginInfo.username = :username")
//                .setParameter("username", username)
//                .getResultList();

    //    List<User> users = getUserByUsername(username);
        //User user=getUserByUsername(username);
      //  if (users == null) throw new RuntimeException();

        User user = getUserByUsername(username);
        user.getPosts().add(post);
        post.setUser(user);

        category.getPosts().add(post);
        post.setCategory(category);

        //document.getPosts().add(post);
        post.setDatetime(new Date());
        if(document!=null) post.setDocument(document);

        em.persist(post);
    }

    public Post getPostByUser(String name) {
        return (Post) em.createNamedQuery("Post.findByUserName", Post.class)
                .setParameter("name", name)
                .getResultList().get(0);

    }

    public void addComment(Comment comment) {
        em.persist(comment);
    }

    public void deleteComment(int commentId) {
        Comment comment = em.find(Comment.class, commentId);
        em.remove(comment);
    }

    public void managed(int commentId) {

        Comment comment = em.find(Comment.class, commentId);
        em.remove(comment);
    }

    public List<User> getAllUsers() {
        return (List<User>) em.createQuery("SELECT u FROM User u")
                .getResultList();
    }

    public List<Post> getAllPosts() {
        return (List<Post>) em.createQuery("SELECT p FROM Post p")
                .getResultList();

    }
}
