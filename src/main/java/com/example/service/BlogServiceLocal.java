package com.example.service;

import com.example.entities.*;

import java.util.List;

/**
 * Created by sonat on 14.08.2016.
 */


public interface BlogServiceLocal {
    void addClient(Client client, User user);

    void removeClient(Client client);

    void updateClient(Client client);

    List<Category> getAllCategories();

    void  addCategory(String name);

    Document addDocument(Document document);

    List<Document> getAllDocuments();

    void addCategory(Category category);

    Category findCategoryByName(String name);

    void addRole(Role role);

    Role findRoleByName(String name);

    void addUser(User user, List<Role> roleList, List<Phone> phones);

    void addPostComment(int postId, Comment comment);

    void addChildComment(int parentId, Comment comment);

    void addCountry(Country country);

    Country getCountryByName(String name);

    void addCity(int countryId, City city);

    City getCityByName(String name);

    User getUserByName(String name);

    User getUserByUsername(String username);

    void addPost(Post post, String categoryName, String username, Document document);

    Post getPostByUser(String name);

    void addComment(Comment comment);

    void deleteComment(int commentId);

    void managed(int commentId);

    List<User> getAllUsers();

    List<Post> getAllPosts();
}
