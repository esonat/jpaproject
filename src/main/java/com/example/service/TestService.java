package com.example.service;

import com.example.entities.TestItem;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

/**
 * Created by sonat on 12.08.2016.
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class TestService {
    @PersistenceUnit
    private EntityManagerFactory emf;
//
//    @Resource
//    private UserTransaction ut;

    public void test(){
        EntityManager entityManager=emf.createEntityManager();

        try{
//            InitialContext ctx=new InitialContext();
//            UserTransaction ut=(UserTransaction)ctx.lookup("java:comp/UserTransaction");

            TestItem item=new TestItem();
            item.setText("deneme");

            entityManager.getTransaction().begin();
           // ut.begin();
            entityManager.persist(item);
            entityManager.getTransaction().commit();
            //ut.commit();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            entityManager.close();
        }
    }
}
