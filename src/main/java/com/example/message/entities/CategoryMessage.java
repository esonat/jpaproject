package com.example.message.entities;

import com.example.entities.Category;

import java.io.Serializable;

/**
 * Created by sonat on 14.08.2016.
 */
public class CategoryMessage extends MessageObject implements Serializable {

    private static final long serialVersionUID = -3711768946070248971L;
    private String name;

    public CategoryMessage(Object object){
        type="Category";
        name=((Category)object).getName();
        message.put(type,name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
