package com.example.message.entities;


import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public abstract class MessageObject implements Serializable {
    String type;
    Map<String,Object> message;

    public MessageObject(){
        message=new HashMap<String, Object>();
    }

    public String getType(){
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Map<String, Object> getMessage() {
        return message;
    }

    public void setMessage(Map<String, Object> message) {
        this.message = message;
    }
}
