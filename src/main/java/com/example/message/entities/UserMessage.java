package com.example.message.entities;

import com.example.dto.InitMapper;
import com.example.entities.User;
import org.dozer.Mapper;

import java.io.Serializable;


public class UserMessage extends MessageObject implements Serializable{
    private String username;

    public UserMessage(Object object){
        Mapper mapper= InitMapper.getMapper();

        username= mapper.map((User)object,String.class,"userMessage");
        type    = "User";
        message.put(type,username);
    }
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
