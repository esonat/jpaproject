//package com.example.message;
//
//import com.example.dto.InitMapper;
//import com.example.entities.Category;
//import com.example.entities.User;
//import com.example.message.entities.CategoryMessage;
//import com.example.message.entities.MessageObject;
//import com.example.message.entities.UserMessage;
//import org.dozer.Mapper;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import java.io.IOException;
//import java.util.concurrent.TimeoutException;
//
//
//public class MessageProducer {
//    public static Logger logger= LoggerFactory.getLogger(MessageProducer.class);
//
//
//
//    public static void sendMessage(Object object) throws IOException{
//
//        try {
//                Producer producer = new Producer("queue");
//
//                MessageObject message=null;
//                Mapper mapper=InitMapper.getMapper();
//
//                if(object.getClass().equals(User.class)){
//                    message = new UserMessage(object);
//                    producer.sendMessage(message);
//                  }
//                if(object.getClass().equals(Category.class)) {
//                    message = new CategoryMessage(object);
//                    producer.sendMessage(message);
//                }
//            logger.info("MESSAGE SENT");
//        } catch (TimeoutException e) {
//            e.printStackTrace();
//        }
//    }
//}