package servlet;

import com.example.dto.InitMapper;
import com.example.entities.*;
import com.example.service.BlogService;
import com.example.view.CategoryView;
import com.example.view.PostView;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.dozer.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import static servlet.DocumentServlet.readFully;


public class PostServlet extends HttpServlet {
  public static Logger logger= LoggerFactory.getLogger(PostServlet.class);

    @EJB
    BlogService blogService;


    public byte[] getDocumentContent(HttpServletRequest request)throws IOException{
        byte[] content=null;

        try {
            DiskFileItemFactory factory = new DiskFileItemFactory();
            ServletFileUpload upload = new ServletFileUpload(factory);

            List<FileItem> items = upload.parseRequest(request);
            FileItem item=items.get(0);

            InputStream in = item.getInputStream();

            content = readFully(in);

        }catch (FileUploadException ex){
            ex.printStackTrace();
        }finally {
            return content;
        }
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String text=null;
        String username=null;
        String catName=null;
        String documentType=null;
        byte[] content=null;

        try {
            DiskFileItemFactory factory = new DiskFileItemFactory();
            ServletFileUpload upload = new ServletFileUpload(factory);

            List<FileItem> items = upload.parseRequest(request);

            for (FileItem item : items) {
                if (item.isFormField()){
                    String name=item.getFieldName();
                    String value=item.getString();

                    if(name.equals("text"))         text=value;
                    if(name.equals("username"))     username=value;
                    if(name.equals("category"))     catName=value;
                    if(name.equals("documentType")) documentType=value;

                }else{
                    InputStream in = item.getInputStream();
                    logger.info("INPUTSTREAM AVAILABLE "+in.available());

                    if(in.available()==0)
                        content=null;
                    else
                        content = readFully(in);
//
//                    if(content==null) logger.info("content null");
//                    else logger.info("content is not null");
                }
            }
        }catch (FileUploadException ex){
            PrintWriter writer=response.getWriter();
            writer.println(ex.getMessage());
        }


            Document document=new Document();
            if(content!=null){
                document.setContent(content);
                document.setType(DocumentType.JPG);
                document.setStatus(DocumentStatus.APPROVED);
            }else document=null;

            Post post=new Post();
            post.setText(text);

            blogService.addPost(post,catName,username,document);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException,IOException
    {

        //Category cat=blogService.findCategoryByName("Java");
        Category cat=new Category();
        cat.setName("Spring");

      //  MessageProducer.sendMessage(cat);


        /*






         */


        String action=request.getParameter("action");

        if(action.equals("list")){
            List<Post> posts=blogService.getAllPosts();
            List<PostView> postList=new ArrayList<PostView>();

            for(Post post:posts){
                Mapper mapper= InitMapper.getMapper();
                PostView postView = (PostView) mapper.map (post, PostView.class,"a");
                postList.add(postView);
            }

            request.setAttribute("postList",postList);

            RequestDispatcher dispatcher=getServletContext().getRequestDispatcher("/posts.jsp");
            dispatcher.forward(request,response);
        }else if(action.equals("add")){

            List<Category>       categories     = blogService.getAllCategories();
            List<CategoryView>   categoryList   = new ArrayList<CategoryView>();

            for(Category category:categories){
                CategoryView categoryView=new CategoryView();
                categoryView.setName(category.getName());

                categoryList.add(categoryView);
            }

            request.setAttribute("categoryList",categoryList);

            RequestDispatcher dispatcher=getServletContext().getRequestDispatcher("/post.jsp");
            dispatcher.forward(request,response);
        }
    }
}
