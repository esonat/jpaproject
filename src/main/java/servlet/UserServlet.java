package servlet;

import com.example.entities.*;
import com.example.service.BlogService;
import com.example.service.TestService;

import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sonat on 10.08.2016.
 */
//@WebServlet(name="UserServlet",urlPatterns = "/UserServlet")
public class UserServlet extends HttpServlet {

    @EJB
    BlogService blogService;
    @EJB
    TestService testService;

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            String name = request.getParameter("name");
            String city = request.getParameter("city");
            String country = request.getParameter("country");
            String street = request.getParameter("street");
            String state = request.getParameter("state");
            String zip = request.getParameter("zip");
            String username = request.getParameter("username");
            String password = request.getParameter("password");
            String phone = request.getParameter("phone");

            List<Phone> phones = new ArrayList<Phone>();
            Phone phone1 = new Phone();
            phone1.setNum(phone);


            City cityItem = blogService.getCityByName(city);

            Address address = new Address();
            address.setCity(cityItem);
            address.setZip(zip);
            address.setStreet(street);
            address.setState(state);

            UserInfo userInfo = new UserInfo();
            userInfo.setName(name);
            userInfo.setAddress(address);
            userInfo.setPhones(phones);

            UserLoginInfo userLoginInfo = new UserLoginInfo();
            userLoginInfo.setUsername(username);
            userLoginInfo.setPassword(password);

            User user = new User();
            user.setUserinfo(userInfo);
            user.setUserLoginInfo(userLoginInfo);

            List<Role> roles = new ArrayList<Role>();
//        Role role=blogService.findRoleByName("Admin");
//        roles.add(role);
            Role admin = new Role();
            admin.setName("Admin");
            Role postUser = new Role();
            postUser.setName("PostUser");

            roles.add(admin);
            roles.add(postUser);

            blogService.addUser(user, roles, phones);
        }catch (EJBException e){
            PrintWriter writer=response.getWriter();
            writer.println(e.getMessage());
            writer.close();
            //response.sendRedirect("/Blog/UserServlet?action=add");
            //doGet(request,response);
//            RequestDispatcher rd=getServletContext().getRequestDispatcher("/user.jsp");
//            rd.forward(request,response);
            //response.sendRedirect("/Blog/UserServlet?list=add");
        }catch (ConstraintViolationException e){
            PrintWriter writer=response.getWriter();
            writer.println(e.getMessage());

            writer.close();
        }
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException,IOException{
       // HttpSession session=request.getSession(true);

        String action = request.getParameter("action");

        if(request.getSession().getAttribute("errorMessage")!=null){
            RequestDispatcher rd=getServletContext().getRequestDispatcher("/user.jsp");
            rd.forward(request,response);
        }

        if(action.equals("list")) {
            List<User> users=blogService.getAllUsers();
            request.setAttribute("users",users);
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/users.jsp");
            rd.forward(request,response);
        }else if(action.equals("add")){
            RequestDispatcher rd=getServletContext().getRequestDispatcher("/user.jsp");
            rd.forward(request,response);
        }else if(action.equals("role")){
            String role="notexists";
            if(blogService.findRoleByName("Admin")!=null){
                role="exists";
            }
            request.setAttribute("role",role);
            RequestDispatcher rd=getServletContext().getRequestDispatcher("/role.jsp");
            rd.forward(request,response);
        }else if(action.equals("test")){
            testService.test();
        }
    }
}
