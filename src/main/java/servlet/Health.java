package servlet;

import com.example.connection.ClientManager;
import com.example.exception.UserNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;



public class Health extends HttpServlet{
    @Inject
    ClientManager manager;

    Logger logger= LoggerFactory.getLogger(Health.class);

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String type     =   request.getParameter("type");
        String clientId =   request.getParameter("id");
        String port     =   request.getParameter("port");
        String username =   request.getParameter("username");
        String role     =   request.getParameter("role");

       PrintWriter out = response.getWriter();

        try{
            manager.addClient(clientId,port,username,role);
        }catch (UserNotFoundException e){
            out.println(e.getMessage());
            out.close();
        }

        out.println("Client: "+clientId+ "connected successfully!");
        out.close();
    }
}
