package servlet;

import com.example.entities.Document;
import com.example.entities.DocumentType;
import com.example.service.BlogService;
import com.example.service.DocumentService;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.slf4j.Logger;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;


public class DocumentServlet extends HttpServlet{
    Logger logger= org.slf4j.LoggerFactory.getLogger(DocumentServlet.class);

    @EJB
    DocumentService documentService;

    @EJB
    BlogService blogService;


    public static byte[] readFully(InputStream input) throws IOException
    {
        byte[] buffer = new byte[8192];
        int bytesRead;
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        while ((bytesRead = input.read(buffer)) != -1)
        {
            output.write(buffer, 0, bytesRead);
        }
        return output.toByteArray();
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException,IOException{

        logger.info("DOCUMENT IS MULTIPART");
        try {
            DiskFileItemFactory factory = new DiskFileItemFactory();
            ServletFileUpload upload = new ServletFileUpload(factory);
            // Parse the request
            List<FileItem> items = upload.parseRequest(request);
            FileItem item=items.get(0);
            //for (FileItem item : items) {
                InputStream in = item.getInputStream();
                //Use in here
                byte[] content = readFully(in);
                String type     =   request.getParameter("type");

                Document document=documentService.setDocumentContent(content);
                documentService.setDocumentType(document, DocumentType.JPG);
                documentService.approveDocument(document);

                logger.info("DOCUMENT APPROVED");
            //}
        }catch (FileUploadException ex){
            PrintWriter writer=response.getWriter();
            writer.println(ex.getMessage());
        }



//        String param=request.getParameter("content");
//        byte[] content=param.getBytes();
//
//        //byte[] content  =   (request.getParameter("content")).getBytes();
//        String type     =   request.getParameter("type");
//
////        Document document=  documentService.setDocumentContent(content);
//        Document doc    =   documentService.setDocumentType(document, DocumentType.valueOf(type));
//        documentService.approveDocument(doc);
    }

    public void doGet(HttpServletRequest request,HttpServletResponse response)
        throws ServletException,IOException{
        String action=request.getParameter("action");

        if(action.equals("add")){
            RequestDispatcher requestDispatcher=getServletContext().getRequestDispatcher("/document.jsp");
            requestDispatcher.forward(request,response);
        }else if(action.equals("list")){
            List<Document> list=blogService.getAllDocuments();
            List<String> contentList=new ArrayList<String>();

            for(Document document:list) {
                String encodedImage = Base64.encode(document.getContent());
                contentList.add(encodedImage);
            }
            request.setAttribute("contentList", contentList);

            RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher("/documents.jsp");
            requestDispatcher.forward(request, response);
        }
    }
}
