<%@ page import="com.example.entities.Post" %>
<%@ page import="com.example.entities.User" %>
<%@ page import="com.example.entities.UserLoginInfo" %>
<%@ page import="java.util.List" %>
<%@ page import="com.example.view.PostView" %>
<%@ page import="com.example.view.CategoryView" %>
<%@ taglib prefix="c"
           uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" session="true" %>
<html>
<head>
    <title>Posts</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
<%
    List<PostView> postList         =(List<PostView>)request.getAttribute("postList");
%>
<div class="jumbotron">
        <%    for(PostView post:postList){%>
                <% if(post.getDocument()!=null) {%>
                    <img id="profileImage" width="300" height="300" src="data:image/jpg;base64,<%=post.getDocument()%>">
                <%}%>
                <div class="form-group"><%=post.getText()%></div>
                <p>Posted on:<%=post.getDatetime()%></p>
                <b style="font-size :40px;"><%=post.getUsername()%></b>
        <% } %>
</div>
</body>
</html>
