<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<div class="jumbotron">
    <% List<String> contentList=(List<String>)request.getAttribute("contentList"); %>

    <%
        for(String content:contentList) {
    %>
        <img id="profileImage" src="data:image/jpg;base64,<%=content%>">
    <%
        }
    %>
</div>

</body>
</html>
